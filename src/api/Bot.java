package api;

/**
 * Interface für einen Bauernschach-Bot.
 * 
 * @author lukas
 * 
 */
public interface Bot {
	/**
	 * Initialisiert den Bot.
	 * 
	 * @param boardSize
	 *            die GrÃ¶sse des Spielfelds.
	 * @param startRow
	 *            die Startreihe fÃ¼r den Bot. FÃ¼r den MaxBot ist das Reihe 0,
	 *            fÃ¼r den MinBot Reihe <code>startRow -1</code>
	 * @param timeInMsPerTurn
	 *            Zeit in ms, die der Bot pro Zug Zeit hat
	 */
	public void init(int boardSize, int startRow, int timeInMsPerTurn);

	/**
	 * Gibt den nächsten Zug des Bots zurück. Muss immer einen gültigen Zug
	 * zurückgeben.
	 * 
	 * @param opponentsMove
	 *            ist immer ein gültiger Zug, nur im ersten Zug des MaxBots ist
	 *            er <code>null</code>
	 * @return
	 */
	public Move nextMove(Move opponentsMove);
}
