package api;

/**
 * ReprÃ¤sentiert ein Feld auf dem Bauernschach-Spielbrett. Der
 * GÃ¼ltigkeitsbereich fÃ¼r row und col betrÃ¤gt [0, boardSize -1]
 * 
 * @author lukas
 * 
 */
public class Tile {
	private int row;
	private int col;

	public Tile(int row, int col) {
		this.row = row;
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	@Override
	public String toString() {
		return "[" + row + ", " + col + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tile other = (Tile) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

}