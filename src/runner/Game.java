package runner;

import api.Move;
import java.util.Arrays;

public class Game {
	private char[][] board;
	private int turn = 0;

	private static final char EMPTY = '_';
	private static final char WHITE = 'W';
	private static final char BLACK = 'B';

	public Game(int boardSize) {
		board = new char[boardSize][boardSize];
		for (char[] row : board) {
			Arrays.fill(row, EMPTY);
		}
		for (int i = 0; i < boardSize; i++) {
			board[0][i] = WHITE;
			board[boardSize - 1][i] = BLACK;
		}
	}

	public void update(Move move, boolean white) {
		if (white) {
			update(move, WHITE);
			turn++;
		} else
			update(move, BLACK);
	}

	private void update(Move move, char player) {
		if (!isLegalMove(move, player)) {
			throw new IllegalArgumentException("Illegal move: "
					+ move.toString());
		}
		board[move.getFrom().getRow()][move.getFrom().getCol()] = EMPTY;
		board[move.getTo().getRow()][move.getTo().getCol()] = player;

		if (isStaleMate())
			throw new IllegalStateException("Stalemate");
	}

	private boolean isStaleMate() {
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				if (board[row][col] == WHITE && canMove(row, col, BLACK))
					return false;
				if (board[row][col] == BLACK && canMove(row, col, WHITE))
					return false;
			}
		}
		return true;
	}

	private boolean canMove(int row, int col, char opponentsColor) {
		final int nextRow = opponentsColor == BLACK ? row + 1 : row - 1;
		if (nextRow < 0 || nextRow >= board.length)
			return false;
		if (board[nextRow][col] == EMPTY)
			return true;
		if (col > 0 && board[nextRow][col - 1] == opponentsColor)
			return true;
		if ((col < board[0].length - 1)
				&& board[nextRow][col + 1] == opponentsColor)
			return true;
		return false;
	}

	private boolean isLegalMove(Move move, char player) {
		final char opponent = player == WHITE ? BLACK : WHITE;
		final boolean straight = move.getTo().getCol() == move.getFrom()
				.getCol();
		final char to = board[move.getTo().getRow()][move.getTo().getCol()];
		if ((straight && (to != EMPTY)) || (!straight && (to != opponent)))
			return false;
		final int rowDiff = move.getFrom().getRow() - move.getTo().getRow();
		final int colDiff = move.getFrom().getCol() - move.getTo().getCol();
		if (rowDiff == 0)
			return false;
		if (Math.abs(rowDiff) > 1 || Math.abs(colDiff) > 1)
			return false;

		return true;
	}

	public int getTurn() {
		return turn;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("Board position (turn " + turn
				+ "): \n");
		for (int i = board.length - 1; i >= 0; i--) {
			for (int j = 0; j < board.length; j++) {
				sb.append(board[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}

}