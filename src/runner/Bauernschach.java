package runner;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import api.Bot;
import api.Move;
import pawndawan.Hans;

public class Bauernschach {

	private Bot white;
	private Bot black;
	private int boardSize;
	// TODO make timeout configurable
	private int timeout = 3000;

	public Bauernschach(Bot bot1, Bot bot2, int boardSize) {
		this.white = bot1;
		this.black = bot2;
		this.boardSize = boardSize;
	}

	public void run() {
		final int startRowWhite = 0;
		final int startRowBlack = boardSize - 1;
		white.init(boardSize, startRowWhite, timeout);
		black.init(boardSize, startRowBlack, timeout);
		Game g = new Game(boardSize);
		Move whiteMove = null;
		Move blackMove = null;
		while (true) {
			whiteMove = callBot(white, blackMove);
			try {
				g.update(whiteMove, true);
			} catch (IllegalArgumentException e) {
				System.out.println("Illegal move by White: "
						+ whiteMove.toString());
			}
			printMove(g, whiteMove, "White");
			if (whiteMove.getTo().getRow() == startRowBlack) {
				System.out.println("Game finished. <"
						+ white.getClass().getSimpleName() + ">(white) won in "
						+ g.getTurn() + " turns.");
				break;
			}

			blackMove = callBot(black, whiteMove);
			try {
				g.update(blackMove, false);
			} catch (IllegalArgumentException e) {
				System.out.println("Illegal move by Black: "
						+ blackMove.toString());
			}
			printMove(g, blackMove, "Black");
			if (blackMove.getTo().getRow() == startRowWhite) {
				System.out.println("Game finished. <"
						+ black.getClass().getSimpleName() + ">(black) won in "
						+ g.getTurn() + " turns.");
				break;
			}
		}
	}

	private Move callBot(final Bot bot, final Move opponentsMove) {
		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<Move> task = new Callable<Move>() {
			public Move call() {
				return bot.nextMove(opponentsMove);
			}
		};
		Future<Move> future = executor.submit(task);
		try {
			return future.get(timeout, TimeUnit.MILLISECONDS);
		} catch (Exception e) {
			System.out.println("Bot " + bot.getClass().getCanonicalName()
					+ " timed out!");
			System.exit(1);
		}
		return null; // should not happen
	}

	private void printMove(Game g, Move move, String bot) {
		System.out.println(bot + ": " + move.toString());
		System.out.println(g.toString());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			int size = 8;
			Bot bot1 = new Hans();
			Bot bot2 = new Hans();
			new Bauernschach(bot1, bot2, size).run();
		} catch (Exception e) {
			e.printStackTrace();
			System.err
					.println("Usage: ant run -Dbot1=<Mybot> -Dbot2=<Yourbot> -Dsize=<boardSize>");
			System.exit(1);
		}
	}

	private static Bot initBot(String botClass) throws Exception {
		@SuppressWarnings("unchecked")
		Class<Bot> clazz = (Class<Bot>) Class.forName(botClass);
		return clazz.newInstance();
	}

}