package pawndawan;

/**
 * Klasse für Heuristische berechnungen der Spielstadien
 * @author fabianschneider
 */
public class Evaluation {

    private Board board;
    private PawnColor myColor;

    public Evaluation(Board board, PawnColor color) {
        this.board = board;
        this.myColor = color;
    }
    
    /**
     * Gibt den heuristischen Wert zurück
     * @return 
     */
    public double evaluate() {
        return this.pawnCount() + this.freeColumns();
    }
    
    /**
     * Zählung der eigenen und der gegnerischen Bauern
     * höherer wert für weniger gegnerische Bauern
     * @return 
     */
    public double pawnCount() {
        double myPawnCount = (double) this.board.getPawns(this.myColor).size();
        double enemyPawnCount = (double) this.board.getPawns(Game.getEnemyColor(this.myColor)).size();
        return myPawnCount + (this.board.getBoardSize() - enemyPawnCount)*2;
    }
    
    /**
     * Zählung der freien Kolonnen
     * je näher der Bauer auf der Freien Kolonne dem Ziel ist so höher der wert
     * @return 
     */
    public double freeColumns() {
        double freeColumns = 0.0;
        for (Pawn pawn : this.board.getPawns(this.myColor)) {
            boolean isFree = true;
            for (Pawn colPawn : this.board.getColumn(pawn.getPosition().getCol())) {

                if (this.myColor.equals(PawnColor.WHITE)) {

                    if (colPawn != null && colPawn.getPosition().getRow() > pawn.getPosition().getRow()) {
                        isFree = false;
                        break;
                    }
                } else {
                    if (colPawn != null && colPawn.getPosition().getRow() < pawn.getPosition().getRow()) {
                        isFree = false;
                        break;
                    }
                }
            }
            if (isFree) {
                 if (this.myColor.equals(PawnColor.WHITE)) {
                     freeColumns += pawn.getPosition().getRow();
                 }else{
                     freeColumns += this.board.getBoardSize()-1-pawn.getPosition().getRow();
                 }
            }
        }
        return freeColumns * 5;
    }
}
