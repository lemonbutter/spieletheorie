package pawndawan;

/**
 * Ein Game Stand mit der Farbe die am Zug war und meiner Farbe
 * @author fabianschneider
 */
public class GameState {
    
    private Game game;
    private Board board;
    
    private double heuristicValue;
    
    private PawnColor turnColor;
    private PawnColor myColor;
    
    public GameState(Board board, PawnColor turnColor, PawnColor myColor, double heuristicValue){
        this.game = new Game(board);
        this.board = board;
        this.heuristicValue = heuristicValue;
        this.turnColor = turnColor;
        this.myColor = myColor;
    }

    public Game getGame(){
        return this.game;
    }

    public double getHeuristicValue() {
        return heuristicValue;
    }
    
    public void setHeuristicValue(double value){
        this.heuristicValue = value;
    }
    
    public double evaluate(int factor){
        return this.heuristicValue = new Evaluation(this.board, this.turnColor).evaluate() * factor;
    }
    
    /**
     * Falls es ein Terminaler Status ist so wird gerechnet ob man gewinnt oder verliert.
     * je grösser der zurückgegebene Wert umso eher verliert man.
     * @param d tiefe das Spielstatus
     * @return 
     */
    public double utility(int d){
        
        for (Pawn pawn : this.board.getPawns(this.turnColor)) {
            if (!pawn.isHit() && pawn.getPosition().getRow() == this.game.getLastRowByColor(Game.getEnemyColor(this.turnColor))) {
                if(this.turnColor == this.myColor) this.heuristicValue = 1000.00*d;
                else this.heuristicValue = -1000.00*d;
                return this.heuristicValue;
            }
        }
        return 0.0;
    }
    
    /**
     * Terminal abfrage
     * @return 
     */
    public boolean terminal(){
        return this.game.hasWinner();
    }

}
