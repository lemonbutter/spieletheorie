package pawndawan;

/**
 * Actionen die ein Bauer machen kann
 * Links/Rechts Schlagen oder ein Feld nach vorne ziehen
 * @author david
 */
public enum PawnAction {
    HIT_LEFT, HIT_RIGHT, MOVE
}
