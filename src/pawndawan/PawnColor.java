package pawndawan;

/**
 * Farben der Bauern
 * @author fabianschneider
 */
public enum PawnColor{
    WHITE, BLACK;
}

