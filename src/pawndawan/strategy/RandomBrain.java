package pawndawan.strategy;

import api.Move;
import java.util.List;
import java.util.Random;
import pawndawan.Game;
import pawndawan.Pawn;
import pawndawan.PawnAction;
import pawndawan.PawnColor;

/**
 * Führt einen zufallsgenerierten Move aus
 * @author baumd9
 */
public class RandomBrain extends AbstractBrain {

    public RandomBrain(PawnColor c, int timeInMsPerTurn) {
        super(c,timeInMsPerTurn);
    }

    @Override
    protected Move processAlgorithm(Game game) {
        List<Pawn> pawns;
        pawns = game.getPawns(this.myColor);
        Pawn pawn = null;
        Random r = new Random();
        while (pawn == null) {
            if (pawns.size() > 0) {
                pawn = pawns.get(r.nextInt(pawns.size()));
                pawns.remove(pawn);
                if (game.canAct(pawn, PawnAction.MOVE)) {
                    return new Move(pawn.getPosition(), game.getNextTile(pawn, PawnAction.MOVE));
                } else if (game.canAct(pawn, PawnAction.HIT_LEFT)) {
                    return new Move(pawn.getPosition(), game.getNextTile(pawn, PawnAction.HIT_LEFT));
                } else if (game.canAct(pawn, PawnAction.HIT_RIGHT)) {
                    return new Move(pawn.getPosition(), game.getNextTile(pawn, PawnAction.HIT_RIGHT));
                } else {
                    pawn = null;
                }
            } else {
                break;
            }
        }
        System.out.println("The " + this.myColor + " player is blocked!");
        return null;
    }
}
