package pawndawan.strategy;

import api.Move;
import pawndawan.Game;
import pawndawan.PawnColor;

public abstract class AbstractBrain implements IPawnChessBrain {

    protected PawnColor myColor;
    protected PawnColor enemyColor;
    protected int startDepth;
    protected int timeInMsPerTurn;
    
    private Move move;

    public AbstractBrain(PawnColor color, int timeInMsPerTurn) {
        this.myColor = color;
        this.enemyColor = Game.getEnemyColor(color);
        this.timeInMsPerTurn = timeInMsPerTurn;
    }

    /**
     * Gibt die Farbe des Spielers zurück
     *
     * @return PawnColor
     */
    public PawnColor getColor() {
        return myColor;
    }

    @Override
    public Move computeMove(Game game){

        return processAlgorithm(game);
    }

    /**
     * Diese Methode startet den Algorithmus
     *
     * @param game Referenz auf das Spiel
     * @return Eine Instanz der Move-Klasse
     */
    protected abstract Move processAlgorithm(Game game);
}
