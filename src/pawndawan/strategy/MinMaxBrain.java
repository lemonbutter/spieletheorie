package pawndawan.strategy;

import api.Move;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import pawndawan.*;
import pawndawan.exeptions.PlayerBlockedException;
import pawndawan.exeptions.TimeIsUpException;

/**
 * MinMax Algorithmus
 * @author baumd9
 */
public class MinMaxBrain extends AbstractBrain {

    private Tree<GameState> tree;
    private int iterationCount;
    private boolean timeIsUp;

    public MinMaxBrain(PawnColor c, int timeInMsPerTurn) {
        super(c, timeInMsPerTurn);
        this.startDepth = 2;
        this.timeIsUp = false;
    }

    @Override
    protected Move processAlgorithm(Game game) {
        Move bestMove = null;
        Double value = 0.0;
        List<Node<GameState>> list = new ArrayList<Node<GameState>>();;
        Random rand = new Random();
        while (!this.timeIsUp) {
            // init für eine neue berechnung
            this.tree = new Tree<GameState>();
            this.iterationCount = 0;
            this.tree.setRootElement(new Node(new GameState(game.getBoard(), this.myColor, this.myColor, 1)));
            try {
                // start minmax algo
                double highest = this.maxi(this.tree.getRootElement(), this.startDepth);
                List<GameState> resultStates = new ArrayList<GameState>();
                // aus den besten resultaten wir zufällig einer gewählt
                for (Node<GameState> child : this.tree.getRootElement().getChildren()) {
                    if (child.getData().getHeuristicValue() == highest) {
                        resultStates.add(child.getData());
                    }
                }
                // wenn resultate existieren, wird der beste move gespeichert
                // sonst wird timeIsUp auf true gesetzt und keine neue iteration mehr gestartet
                if(resultStates.size() > 0){
                    int index = rand.nextInt(resultStates.size());
                    bestMove = resultStates.get(index).getGame().getLastMove();
                    value = resultStates.get(index).getHeuristicValue();
                }else{
                    this.timeIsUp = true;
                }
//                list = this.tree.getRootElement().getChildren();
                // start tiefe für die nächste iteration hochzählen
                this.startDepth += 2;
            } catch (TimeIsUpException e) {
                this.timeIsUp = true;
            } catch (PlayerBlockedException pbe){
                System.out.println(pbe.getMessage());
                this.timeIsUp = true;
            }
        }
//        System.out.println("HighestVal-" + (startDepth - 2) +": " + value);
//        for (Node<GameState> child : list) {
//            System.out.print(","+child.getData().getHeuristicValue());
//        }
//        System.out.println("");
        // starttiefe und timer variabeln zurücksetzen für den nächsten move
        this.startDepth = 2;
        this.timeIsUp = false;
        return bestMove;
    }
    
    /**
     * MAX knoten
     * @param node aktueller knoten
     * @param d aktuelle tiefe
     * @return highest höchster heuristischer wert
     * @throws TimeIsUpException
     * @throws PlayerBlockedException 
     */
    private double maxi(Node<GameState> node, int d) throws TimeIsUpException, PlayerBlockedException {
        GameState gameState = node.getData();
        // check for time
        if (!Game.timeIsUp()) {
            if (d == 0) {
                return gameState.evaluate(1);
            } else if (gameState.terminal()) {
                return gameState.utility(d);
            }
            //expand game
            List<Board> expanded = gameState.getGame().expand(this.myColor);
            // check if i'm blocked
            if(this.iterationCount == 1 && expanded.isEmpty()) throw new PlayerBlockedException("blocked(" + this.myColor + ")");
            double highest = Double.NEGATIVE_INFINITY;
            for (Board board : expanded) {
                Node<GameState> child = new Node(new GameState(board, this.myColor, this.myColor, -1));
                node.addChild(child);
                double score = this.mini(child, d - 1);
                if(score > highest){
                    highest = score;
                }
            }
            // ------------------
            node.getData().setHeuristicValue(highest);
            if (d != this.startDepth) {
                node.setChildren(null);
            }
            return highest;
        } else {
            throw new TimeIsUpException("Time is up buddy!");
        }
    }
    /**
     * MIN Knoten
     * @param node aktueller knoten
     * @param d aktuelle tiefe
     * @return lowest tiefste heuristischer wert
     * @throws TimeIsUpException
     * @throws PlayerBlockedException 
     */
    private double mini(Node<GameState> node, int d) throws TimeIsUpException, PlayerBlockedException {
        GameState gameState = node.getData();
        // check for time
        if (!Game.timeIsUp()) {
            if (d == 0) {
                return gameState.evaluate(-1);
            } else if (gameState.terminal()) {
                return gameState.utility(d);
            }
            double lowest = Double.POSITIVE_INFINITY;
            for (Board board : gameState.getGame().expand(this.enemyColor)) {
                Node<GameState> child = new Node(new GameState(board, this.enemyColor, this.myColor, 1));
                node.addChild(child);
                double score = this.maxi(child, d - 1);
                if(score < lowest){
                    lowest = score;
                }
            }
            // ------------------
            node.getData().setHeuristicValue(lowest);
            if (d != this.startDepth) {
                node.setChildren(null);
            }
            return lowest;
        } else {
            throw new TimeIsUpException("Time is up buddy!");
        }
    }
}