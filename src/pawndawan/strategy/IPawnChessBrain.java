package pawndawan.strategy;

import api.Move;
import pawndawan.Game;

/**
 * Interface für unsere Algorithmen
 *
 * @author baumd9
 */
public interface IPawnChessBrain {

    /**
     * Beim aufruf von nextMove wird diese Methode angestossen.
     *
     * @param game Referenz auf das aktuelle Spiel
     * @return Instanz der Move-Klasse mit dem besten Move
     */
    public Move computeMove(Game game);
}
