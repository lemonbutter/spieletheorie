/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pawndawan.exeptions;

/**
 *
 * @author david
 */
public class TimeIsUpException extends Exception{
    public TimeIsUpException(){
        super();
    }
    
    public TimeIsUpException(String msg){
        super(msg);
    }
}
