/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pawndawan.exeptions;

/**
 *
 * @author david
 */
public class PlayerBlockedException extends Exception{
    public PlayerBlockedException(){
        super();
    }
    
    public PlayerBlockedException(String msg){
        super(msg);
    }
}
