package pawndawan;

import api.Bot;
import api.Move;
import pawndawan.strategy.AlphaBetaBrain;
import pawndawan.strategy.IPawnChessBrain;
import pawndawan.strategy.MinMaxBrain;
import pawndawan.strategy.RandomBrain;

/**
 *
 * @author fabianschneider
 */
public class Hans implements Bot {
    
    private Game game;
    private IPawnChessBrain brain;
    private PawnColor botColor;
    private int timeInMsPerTurn;

    @Override
    public void init(int boardSize, int startRow, int timeInMsPerTurn) {
        this.game = new Game(boardSize,timeInMsPerTurn);
        if(startRow == 0) this.botColor = PawnColor.WHITE;
        else this.botColor = PawnColor.BLACK;
//        this.brain = new RandomBrain(this.botColor, this.timeInMsPerTurn);
//        this.brain = new MinMaxBrain(this.botColor, this.timeInMsPerTurn);
        this.brain = new AlphaBetaBrain(this.botColor, this.timeInMsPerTurn);
        this.timeInMsPerTurn = timeInMsPerTurn;
    }

    @Override
    public Move nextMove(Move opponentsMove){
        // start timer
        Game.startTime();
        // update game
        if( opponentsMove != null) this.game.updateBoard(opponentsMove);
        // compute the next move
        Move nextMove = this.brain.computeMove(this.game);
        this.game.updateBoard(nextMove);
        return nextMove;
    }
    
}
