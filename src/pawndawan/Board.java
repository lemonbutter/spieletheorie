package pawndawan;

import api.Move;
import api.Tile;
import java.util.ArrayList;
import java.util.List;

/**
 * Das Spielbrett
 * @author fabianschneider
 */
public class Board {

    private Pawn[][] board;
    private List<Pawn> whitePawns;
    private List<Pawn> blackPawns;
    private int boardSize;
    private Move lastMove;
    
    /**
     * Contructor für ein geklontes Spielbrett
     * @param board
     * @param white
     * @param black
     * @param lastMove
     * @param boardSize 
     */
    public Board(Pawn[][] board, List<Pawn> white, List<Pawn> black, Move lastMove, int boardSize) {
        this.board = board;
        this.whitePawns = white;
        this.blackPawns = black;
        this.boardSize = boardSize;
    }
    
    /**
     * Constructor für das Spielbrett beim start des spiels
     * Das Spielbrett wird initialisiert
     * @param boardSize 
     */
    public Board(int boardSize) {
        this.board = new Pawn[boardSize][boardSize];
        this.boardSize = boardSize;
        this.whitePawns = new ArrayList<Pawn>();
        this.blackPawns = new ArrayList<Pawn>();
        this.initBoard();
    }
    
    /**
     * Methode zum initialisieren des Spielbretts
     * Weisse und Schwarte Bauern werden an ihre Startpositionen gesetzt.
     */
    private void initBoard() {
        // init white pawns
        for (int i = 0; i < this.boardSize; i++) {
            Pawn pawn = new Pawn(PawnColor.WHITE, new Tile(0, i));
            this.board[0][i] = pawn;
            this.whitePawns.add(pawn);
        }
        // init black pawns
        for (int i = 0; i < this.boardSize; i++) {
            Pawn pawn = new Pawn(PawnColor.BLACK, new Tile(this.boardSize - 1, i));
            this.board[this.boardSize - 1][i] = pawn;
            this.blackPawns.add(pawn);
        }
    }
    
    /**
     * Gibt die Bauern der Farbe color zurück
     * @param color
     * @return 
     */
    public List<Pawn> getPawns(PawnColor color) {
        List<Pawn> pawns = new ArrayList<Pawn>();
        if (color.equals(PawnColor.WHITE)) {
            for (Pawn pawn : this.whitePawns) {
                if (!pawn.isHit()) {
                    pawns.add(pawn);
                }
            }
        } else {
            for (Pawn pawn : this.blackPawns) {
                if (!pawn.isHit()) {
                    pawns.add(pawn);
                }
            }
        }
        return pawns;
    }

    /**
     * Gibt eine Reihe des Spielbrettes zurück
     * benötigt für FreeLines Check
     */
    public Pawn[] getColumn(int col) {
        return this.board[col];
    }
    
    /**
     * Gibt den Bauern auf dem übergebenen Spielfeld zurück, falls einer darauf existiert
     * @param tile
     * @return 
     */
    public Pawn getPawn(Tile tile) {
        return this.board[tile.getRow()][tile.getCol()];
    }
    
    /**
     * Setzt einen Bauern auf dasübergebene Spielfeld und gibt den Bauern der darauf war zurück,
     * falls da ein Bauer war.
     * @param pawn
     * @param tile
     * @return 
     */
    public Pawn setPawn(Pawn pawn, Tile tile) {
        Pawn hitPawn = this.board[tile.getRow()][tile.getCol()];
        this.board[tile.getRow()][tile.getCol()] = pawn;
        return hitPawn;
    }
    
    public int getBoardSize() {
        return this.boardSize;
    }

    public Move getLastMove() {
        return this.lastMove;
    }
    
    /**
     * Abfrage ob ein Spielfeld leer ist
     * @param tile
     * @return 
     */
    public boolean isTileEmpty(Tile tile) {
        if (board[tile.getRow()][tile.getCol()] == null) {
            return true;
        }
        return false;
    }
    
    /**
     * Abfrage ob sich auf einem Spielfeld ein gegnerischer Bauer befindet
     * @param tile
     * @param pawn
     * @return 
     */
    public boolean isTargetEnemy(Tile tile, Pawn pawn) {
        if (!this.getPawn(tile).getColor().equals(pawn.getColor())) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Das Board wird mit einem Move aktualisiert, allfällige geschlagene Bauern werden als geschlagen markiert
     * @param move 
     */
    public void updateBoard(Move move) {
        Pawn pawnToMove = this.getPawn(move.getFrom());
        this.setPawn(null, move.getFrom());
        Pawn hitPawn = this.setPawn(pawnToMove, move.getTo());
        if (hitPawn != null) {
            hitPawn.hit();
        }
        pawnToMove.setPosition(move.getTo());
        this.lastMove = move;
    }

    @Override
    protected Board clone() {

        Pawn[][] clonedBoard = new Pawn[this.boardSize][this.boardSize];
        List<Pawn> clonedWhitePawns = new ArrayList<Pawn>();
        List<Pawn> clonedBlackPawns = new ArrayList<Pawn>();

        for (Pawn p : this.whitePawns) {
            Pawn clonedPawn = p.clone();
            clonedWhitePawns.add(clonedPawn);
            if (!clonedPawn.isHit()) {
                clonedBoard[p.getPosition().getRow()][p.getPosition().getCol()] = clonedPawn;
            }
        }
        for (Pawn p : this.blackPawns) {
            Pawn clonedPawn = p.clone();
            clonedBlackPawns.add(clonedPawn);
            if (!clonedPawn.isHit()) {
                clonedBoard[p.getPosition().getRow()][p.getPosition().getCol()] = clonedPawn;
            }
        }

        return new Board(clonedBoard, clonedWhitePawns, clonedBlackPawns, this.lastMove, this.boardSize);
    }
}
