/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pawndawan;

import api.Move;
import api.Tile;
import java.util.ArrayList;
import java.util.List;
import pawndawan.exeptions.PlayerBlockedException;

/**
 *
 * @author fabianschneider
 */
public class Game {

    private Board board;
    //   private int turn = 1;
    private static int maxTime;
    private static long startTime;
    private PawnColor winnerColor;
    

    public Game(int boardSize,int maxTime) {
        this.board = new Board(boardSize);
        Game.maxTime = maxTime;
    }

    public Game(Board board) {
        this.board = board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Board getBoard() {
        return this.board;
    }
    
    public PawnColor getWinnerColor(){
        return this.winnerColor;
    }

    public List<Pawn> getPawns(PawnColor color) {
        return this.board.getPawns(color);
    }

    public void updateBoard(Move move){
        this.board.updateBoard(move);
        if (this.hasWinner()) {
            //runner is checking
        }
    }
    
    /**
     * Abfrage ob ein Bauer den gewünschten move ausführen kann
     * @param pawn
     * @param action
     * @return 
     */
    public boolean canAct(Pawn pawn, PawnAction action) {
        if (pawn.isHit()) {
            return false;
        }
        Tile tile = this.getNextTile(pawn, action);

        if (tile != null) {
            if (action == PawnAction.MOVE) {
                return this.board.isTileEmpty(tile);
            } else if (action == PawnAction.HIT_LEFT || action == PawnAction.HIT_RIGHT) {
                return (!this.isTileEmpty(tile) && this.isTargetEnemy(tile, pawn));
            }
        }


        if (tile != null && this.board.isTileEmpty(tile)) {
            return true;
        }
        return false;
    }
    
    /**
     * Gibt das nächste Spielfeld anhand des gewünschten moves zurück
     * @param pawn
     * @param action
     * @return 
     */
    public Tile getNextTile(Pawn pawn, PawnAction action) {

        Tile startTile = pawn.getPosition();
        int vertical = (pawn.getColor() == PawnColor.BLACK) ? -1 : 1;
        int horizontal = 0;

        if (action != PawnAction.MOVE) {
            horizontal = (pawn.getColor() == PawnColor.BLACK) ? -1 : 1;
            if (action == PawnAction.HIT_RIGHT) {
                horizontal *= -1;
            }
        }

        int targetRow = startTile.getRow() + vertical;
        int targetCol = startTile.getCol() + horizontal;

        if (targetRow < 0 || targetRow > this.board.getBoardSize() - 1 || targetCol < 0 || targetCol > this.board.getBoardSize() - 1) {
            return null;
        }
        return new Tile(targetRow, targetCol);
    }
    
    /**
     * @param tile
     * @see pawndawan.Board.isTileEmpty
     * @return 
     */
    public boolean isTileEmpty(Tile tile) {
        return this.board.isTileEmpty(tile);
    }
    
    /**
     * @param tile
     * @param pawn
     * @see pawndawan.Board.isTargetEnemy
     * @return 
     */
    public boolean isTargetEnemy(Tile tile, Pawn pawn) {
        return this.board.isTargetEnemy(tile, pawn);
    }
    
    /**
     * Gibt die nächste Reihe des Bauern zurück
     * @param pawn
     * @return 
     */
    public int getNextRowOfPawn(Pawn pawn) {
        if (!pawn.isHit()) {
            return pawn.getPosition().getRow() + ((pawn.getColor() == PawnColor.BLACK) ? -1 : 1);
        }
        return -1;
    }
    
    /**
     * Gibt die letzte reihe auf dem Spielfeld der jeweiligen Farbe zurück
     * @param color
     * @return 
     */
    public int getLastRowByColor(PawnColor color) {
        if (color.equals(PawnColor.WHITE)) {
            return 0;
        } else {
            return this.board.getBoardSize() - 1;
        }
    }
    
    /**
     * Gibt die Farbe des Gegners aus
     * @param color
     * @return 
     */
    public static PawnColor getEnemyColor(PawnColor color) {
        if (color.equals(PawnColor.BLACK)) {
            return PawnColor.WHITE;
        } else {
            return PawnColor.BLACK;
        }
    }

    public Move getLastMove() {
        return this.board.getLastMove();
    }
    
    /**
     * Gibt alle möglichen Moves zurück
     * @param moveColor
     * @return 
     */
    public List<Move> getPossibleMoves(PawnColor moveColor) {

        List<Move> possibleMoves = new ArrayList<Move>();

        List<Pawn> movePawns = this.board.getPawns(moveColor);

        for (Pawn pawn : movePawns) {
            if (this.canAct(pawn, PawnAction.MOVE)) {
                possibleMoves.add(new Move(pawn.getPosition(), this.getNextTile(pawn, PawnAction.MOVE)));
            }
            if (this.canAct(pawn, PawnAction.HIT_LEFT)) {
                possibleMoves.add(new Move(pawn.getPosition(), this.getNextTile(pawn, PawnAction.HIT_LEFT)));
            }
            if (this.canAct(pawn, PawnAction.HIT_RIGHT)) {
                possibleMoves.add(new Move(pawn.getPosition(), this.getNextTile(pawn, PawnAction.HIT_RIGHT)));
            }
        }
        return possibleMoves;
    }
    
    /**
     * Expandieren des Games
     * für jeden möglichen zug wird das Board geklont, aktualisiert und in einen gameState geschrieben
     * @param color
     * @return
     * @throws PlayerBlockedException 
     */
    public List<Board> expand(PawnColor color) throws PlayerBlockedException {

        List<Board> gameStates = new ArrayList<Board>();

        List<Move> possibleMoves = this.getPossibleMoves(color);

        for (Move move : possibleMoves) {
            Board stateBoard = this.board.clone();
            stateBoard.updateBoard(move);
            gameStates.add(stateBoard);
        }
        return gameStates;
    }

    /**
     * Abfrage ob es einen Gewinner gibt
     * @return 
     */
    public boolean hasWinner() {
        int lastRowColor = this.getLastRowByColor(PawnColor.WHITE);
        for (Pawn blackPawn : this.board.getPawns(PawnColor.BLACK)) {
            if (!blackPawn.isHit() && blackPawn.getPosition().getRow() == lastRowColor) {
                this.winnerColor = PawnColor.BLACK;
                return true;
            }
        }
        lastRowColor = this.getLastRowByColor(PawnColor.BLACK);
        for (Pawn whitePawn : this.board.getPawns(PawnColor.WHITE)) {
            if (!whitePawn.isHit() && whitePawn.getPosition().getRow() == lastRowColor) {
                this.winnerColor = PawnColor.WHITE;
                return true;
            }
        }
        return false;
    }
    
    /**
     * Zeit berechnung
     * @return 
     */
    public static boolean timeIsUp(){
        int timeRange = 100;
        if(System.currentTimeMillis() - Game.startTime > Game.maxTime-timeRange){
            return true;
        }
        return false;
    }
    
    /**
     * Setzen der Startzeit für die Zeitberechnung
     */
    public static void startTime(){
        Game.startTime = System.currentTimeMillis();
    }
}
