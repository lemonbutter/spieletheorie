package pawndawan;

import api.Tile;

/**
 * Ein Bauer mit seiner Farbe und Position
 * @author fabianschneider
 */
public class Pawn {
 
    private PawnColor color;
    private Tile position;
    
    public Pawn(PawnColor color, Tile startPosition){
        this.color = color;
        this.position = startPosition;
    }
    
    public PawnColor getColor(){
        return this.color;
    }
    
    public void setPosition(Tile position){
        this.position = position;
    }
    
    public Tile getPosition(){
        return this.position;
    }
    
    public boolean isHit(){
        return position == null;
    }
    
    public void hit(){
        this.position = null;
    }

    @Override
    protected Pawn clone() {
        return new Pawn(this.color, this.position);
    }
    
    
}